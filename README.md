```
#!c++

#include <iostream>

using namespace std;
int main(int argc, char **argv)
{
    cout << "This readme needs improvement";
}
```

#S(h)i(t)mulator in c++
This readme explains all features, every features can run on its own in combination with the _Main_ class.
##Main ticker
The file `main.cpp` does all the threading/ticking for the simulator.
Tick interval is 100ms.

##Logging
###Usage
Initialisation: `Log logger(int consoleLevelOfOutputting);`

Logging a message: `logger.in(String message, int logLevel);`

Clearing previously written messages: `logger.clear();`
###Log output
After a `.in` has been executed the Log class will output the message in console based on its logging level.

The message shall always be logged in the *.log file.
###Log levels
* **1** - Errors
    * Critical errors and failed stuff.
* **2** - Warnings
    * Events that really should be looked at.
* **3** - Notice
* **4** - Informative
    * Default level of console-outputting, this log-level should display things like '_World created, its 12pm._'.
* **5** - Debug
    * Displays everything including verbose/dev log messages.
    
    

**Please do not import** `Agent-Smith.cpp`

![alt text](http://thelycaeum.files.wordpress.com/2014/02/agent-smith.jpg "Agent Smith")