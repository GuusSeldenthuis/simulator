#include <list>
#include "utils/time.h"
#include "utils/log.h"

#include "human.cpp"

class World {
private:
    int worldNumber;
    long worldStart;
    std::list<Human *> residents;

public:
    World(int input) : worldNumber(input) {
        // Constructor for world.
        worldStart = Time::getHostTime();
    }

    int getWorldNumber() {
        return worldNumber;
    }

    /**
     * Get the since-epoch seconds of world creation.
     * @return Long-type.
     */
    long getWorldStart() const {
        return worldStart;
    }


    void addResident() {
        residents.push_front(new Human());
        Log::in("Human \"" + residents.front()->getName() + "\" created.", 4);
    }

    void listResidents() {
        for (std::list<Human *>::iterator residentIterator = residents.begin();
             residentIterator != residents.end();
             ++residentIterator) {
            Human *iteratedHumanPointer = *residentIterator;
            Log::in("[" + std::to_string(iteratedHumanPointer->getSex()) + "] " + iteratedHumanPointer->getName() +
                    " age: " + std::to_string(iteratedHumanPointer->getAge()), 4);
        }
    }

    void tick() {
        for (std::list<Human *>::iterator residentIterator = residents.begin();
             residentIterator != residents.end();
             ++residentIterator) {
            Human *iteratedHumanPointer = *residentIterator;
            iteratedHumanPointer->checkStatus();
        }
    }

};
