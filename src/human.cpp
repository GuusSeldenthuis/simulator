#include <string>
#include "utils/time.h"
#include "utils/log.h"

class Human {
public:
    /**
     * Constructor for creating new persons.
     * @return A newly born Human object.
     */
    Human() {
        // Settings birthday to now.
        birthDate = Time::getHostTime();
        // If the time of birth is odd, the sex is female,
        // else by default the sex is male.
        if (birthDate % 2 != 0) sex = 'f';
    }

    /**
     * Deconstructor (When the human dies for example.)
     * @return Message.
     */
    ~Human() {
        Log::in(getName() + " has been removed from his world.", 4);
    }

    /**
     * Calcuates the Human's age based on date of birth.
     * @return Amount of milliseconds the Human is alive.
     */
    long getAge() const {
        return (Time::getHostTime() - birthDate);
    }

    /**
     * Returns the Human's full name.
     * @return
     */
    std::string getName() const {
        return name;
    }

    char getSex() const {
        return sex;
    }

    /**
     * Used in the world's tick.
     */
    void checkStatus() {
        // If the person is 10 minutes old let it die.
        if (getAge() > (10 * 60 * 1000)) {

        }
    }

private:
    std::string name;
    long birthDate;
    char sex = 'm';
};