//
// Created by guus on 11/5/16.
//
#include <iostream>
#include <fstream>
#include <ctime>
#include "log.h"

using namespace std;

void Log::in(std::string logMessage, int logLevel = 4) {

    // Prepare the timestamp
    time_t now = time(0);

    // Formatting the log message.
    std::string formattedLogMessage =
            "[" + std::to_string(logLevel) + "][" + std::to_string(now) + "] " + logMessage + "\n";

    // Outputs the message into console.
    if (logLevel <= 4) {
        cout << formattedLogMessage;
    }

    ofstream logFile;
    // appending.
    logFile.open("log.log", ios::app);
    logFile << formattedLogMessage;
    logFile.close();
}

void Log::clear() {
    ofstream logFile;
    // writing nothing in log clears it.
    logFile.open("log.log");
    logFile << "";
    logFile.close();
}