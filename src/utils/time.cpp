//
// Created by guus on 11/5/16.
//

#include <sys/time.h>
#include <cstdio>
#include "time.h"


long Time::getHostTime() {
    timeval time;
    gettimeofday(&time, NULL);
    return (time.tv_sec * 1000) + (time.tv_usec / 1000);
}