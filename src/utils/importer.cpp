//
// Created by guus on 11/5/16.
//

#include <fstream>
#include <iterator>
#include <vector>
#include <iostream>
#include "log.h"
#include "importer.h"

Importer::Importer() {
    Log::in("importer started importing files.", 3);


    std::ifstream is("/home/guus/projects/cpp/main/simulator/seeds/names/male.txt");
    std::istream_iterator<std::string> start(is), end;
    std::vector<std::string> maleNames(start, end);

    Log::in(std::to_string(maleNames.size()) + " amount of names loaded.", 4);
}

std::string Importer::getRand(int) {
    return std::string();
}

std::vector<std::string> Importer::getList(int) {
    return vector<string>();
}
