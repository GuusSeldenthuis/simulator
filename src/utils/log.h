//
// Created by guus on 11/5/16.
//
#include <iostream>
#include <fstream>

#ifndef SIMULATOR_LOG_H
#define SIMULATOR_LOG_H
using namespace std;
namespace Log {
    static void in(std::string, int);

    static void clear();
};

#endif //SIMULATOR_LOG_H
