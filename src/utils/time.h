//
// Created by guus on 11/5/16.
//

#ifndef SIMULATOR_TIME_H
#define SIMULATOR_TIME_H


namespace Time {
    static long getHostTime();
};

#endif //SIMULATOR_TIME_H
