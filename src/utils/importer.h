//
// Created by guus on 11/6/16.
//

#ifndef SIMULATOR_IMPORTER_H
#define SIMULATOR_IMPORTER_H

#include <string>
#include <vector>

class Importer {
public:
    void Importer();

    static std::string getRand(int);

    std::vector<std::string> getList(int);
};

#endif //SIMULATOR_IMPORTER_H
