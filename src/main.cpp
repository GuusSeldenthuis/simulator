#include <chrono>
#include <thread>
#include "world.cpp"
#include "utils/log.cpp"
#include "utils/time.cpp"

int main(int argc, char **argv) {
    using namespace std::chrono;

    Log::clear();

    World theWorld(1337);

    std::string logMessage = "World created at: " + std::to_string(theWorld.getWorldStart());
    Log::in(logMessage);

    long tickStart = 0;
    long tickEnd = 0;
    long tickTime = 100;
    long sleepTillNextTick = 0;
    int amountOfPeopleBorn = 0;

    while (true) {
        tickStart = Time::getHostTime();

        // Main method for making "the world work".
        theWorld.tick();
        //
        // Plant some humans.
        while (amountOfPeopleBorn != 100) {
            theWorld.addResident();
            amountOfPeopleBorn++;
        }

        if (tickStart > (theWorld.getWorldStart() + 3000)) {
            theWorld.listResidents();
            break;
        }

        // Calculate how long to wait until the next tick.
        tickEnd = Time::getHostTime();
        sleepTillNextTick = tickTime - (tickEnd - tickStart);
        std::this_thread::sleep_for(std::chrono::milliseconds(sleepTillNextTick));
    }


}
